import librosa
from numpy import argsort
from panns_inference import AudioTagging, labels


class AudioClassifier:
    shotProbableClasses = [
        "Explosion",
        "Gunshot, gunfire",
        "Machine gun",
        "Artillery fire",
        "Burst, pop",
    ]

    def __init__(self, device: str = "cuda", topk: int = 5, treshold: float = 0.25):
        """
        For more models please visit: https://github.com/qiuqiangkong/panns_inference

        This classifier answering the question: "Is there any sound class in the audio
        that is shot probable?"

        If the restul of 'detect' function is 'True', for further analization
        'SoundEventDetection' from 'panns_inferece' might be used, to find the frames (time range)
        of the detected event.

        Parameters
        ----------
        device: str
            The argument should be 'cpu' or 'cuda'
        topk: int
            Amount of top predicted models to be used in detection
        treshold: float
            Value in range [0.01,1]

        """
        if treshold < 0.01 or treshold > 1:
            raise ValueError("Value of treshold should be in range [0.01, 1]")

        self.device = device
        self.topk = topk
        self.treshold = treshold

    def getAudioTaggingResults(self, clipwise_output):
        sorted_indexes = argsort(clipwise_output)[::-1]
        return {
            labels[sorted_indexes[idx]]: clipwise_output[sorted_indexes[idx]]
            for idx in range(self.topk)
        }

    def detect(self, path_to_audio: str = None):
        audio, _ = librosa.core.load(path_to_audio, sr=32000, mono=True)
        audio = audio[None, :]  # (batch_size, segment_samples)
        audioTagging = AudioTagging(checkpoint_path=None, device=self.device)
        clipwise_output, embedding = audioTagging.inference(audio)
        return self.isShotProbable(clipwise_output[0])

    def isShotProbable(self, clipwise_output):
        audioTags = self.getAudioTaggingResults(clipwise_output)
        for tag in audioTags.keys():
            if tag in self.shotProbableClasses and audioTags[tag] >= self.treshold:
                return True
        return False
