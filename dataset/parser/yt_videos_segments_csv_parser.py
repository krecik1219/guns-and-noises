import pandas as pd

from dataset.parser.csv_parser_interface import CsvParserInterface


class YtVideosSegmentsCsvParser(CsvParserInterface):
    def parse_csv(self, path_to_csv: str) -> pd.DataFrame:
        return pd.read_csv(
            path_to_csv,
            header=None,
            names=("yt_id", "segment_start_seconds", "segment_end_seconds"),
            usecols=[0, 1, 2],
            dtype={
                "yt_id": str,
                "segment_start_seconds": float,
                "segment_end_seconds": float,
            },
        )
