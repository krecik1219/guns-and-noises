import itertools
from abc import abstractmethod
from dataclasses import dataclass
from functools import partial
from typing import Any

import pandas as pd

from dataset.parser.csv_parser_interface import CsvParserInterface
from dataset.parser.yt_videos_segments_csv_parser import YtVideosSegmentsCsvParser
from dataset.video.downloader.segment_downloader_interface import (
    SegmentDownloaderInterface,
)
from dataset.video.downloader.supplementary_types import DownloaderOptions
from dataset.video.downloader.yt_segment_downloader import YtSegmentDownloader
from logger import logger
from utils.archiver import Archiver


@dataclass
class ProcessorConfiguration(object):
    videos_per_compression_batch: int = 10
    frames_per_second: int = 1
    start_offset: int = 0
    end_offset: int = 0


class DatasetProcessorBase(object):
    def __init__(
        self,
        yt_cookiefile_path=None,
        configuration: ProcessorConfiguration = ProcessorConfiguration(),
        csv_parser: CsvParserInterface = YtVideosSegmentsCsvParser(),
        segment_downloader: SegmentDownloaderInterface = YtSegmentDownloader(),
        downloader_options: DownloaderOptions = DownloaderOptions(),
        archiver=Archiver(),
    ):
        self._configuration = configuration
        self._csv_parser = csv_parser
        self._segment_downloader = segment_downloader
        self._segment_downloader.set_cookiefile(yt_cookiefile_path)
        self._downloader_options = downloader_options
        self._archiver = archiver
        self._batch_counter = 0
        self._failed_to_process_list = []

    def process_dataset(self, path_to_videos_csv, output_dir_path):
        self._batch_counter = 0
        self._failed_to_process_list = []
        videos_to_convert = self._csv_parser.parse_csv(path_to_videos_csv)
        self._apply_offsets(videos_to_convert)
        videos_groups = self._group_videos(videos_to_convert)
        for video_group in videos_groups:
            try:
                self._process_video_group(video_group, output_dir_path)
            except Exception as e:
                logger.error(f"process_dataset() Error: {e}")

        logger.info(
            f"process_dataset() Failed to process list:\n{self._failed_to_process_list}"
        )

    def _apply_offsets(self, videos_to_convert):
        videos_to_convert["segment_start_seconds"] = (
            videos_to_convert["segment_start_seconds"]
            + self._configuration.start_offset
        )
        videos_to_convert["segment_end_seconds"] = (
            videos_to_convert["segment_end_seconds"] + self._configuration.end_offset
        )

    def _group_videos(self, videos_to_convert):
        videos_count = videos_to_convert.shape[0]
        num_of_compression_batches = (
            videos_count // self._configuration.videos_per_compression_batch
        )
        end_row = 0
        for i in range(0, num_of_compression_batches):
            starting_row = i * self._configuration.videos_per_compression_batch
            end_row = (i + 1) * self._configuration.videos_per_compression_batch
            logger.debug(f"Next group from {starting_row} to {end_row}")
            yield videos_to_convert.iloc()[starting_row:end_row, :]

        if videos_count > end_row:
            logger.debug(f"Next group from {end_row} to {videos_count}")
            yield videos_to_convert.iloc()[end_row:videos_count, :]

    def _download_video(
        self, yt_id, segment_start_seconds, segment_end_seconds, output_dir_path
    ):
        logger.debug(
            f"yt_id: {yt_id}, segment_start_seconds: {segment_start_seconds},"
            f" segment_end_seconds: {segment_end_seconds}, output_dir_path: {output_dir_path}"
        )
        yt_url = "https://youtu.be/" + yt_id
        return self._segment_downloader.download(
            yt_id,
            yt_url,
            segment_start_seconds,
            segment_end_seconds,
            DownloaderOptions(
                output_dir_path=output_dir_path,
                download_video=self._downloader_options.download_video,
                download_audio=self._downloader_options.download_audio,
            ),
        )

    @abstractmethod
    def _process_single_video(
        self, output_dir_path: str, data_row: pd.DataFrame
    ) -> Any:
        pass

    def _process_video_group(self, video_group, output_dir_path):
        outputs = video_group.apply(
            partial(self._process_single_video, output_dir_path), axis=1
        )
        outputs = [output for output in outputs if output is not None and output != []]
        if any(isinstance(el, list) for el in outputs):
            outputs_flattened = list(itertools.chain.from_iterable(outputs))
        else:
            outputs_flattened = outputs
        self._postprocess(outputs_flattened, output_dir_path)
        self._batch_counter += 1

    def _postprocess(self, processing_outputs, output_dir_path):
        pass
