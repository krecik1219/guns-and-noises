import os
from dataclasses import dataclass


class MediaMergingError(Exception):
    pass


class DownloadError(Exception):
    pass


class StreamsInconsistency(Exception):
    pass


@dataclass
class DownloaderOptions(object):
    output_dir_path: os.path = os.path.abspath(".")
    download_video: bool = True
    download_audio: bool = False


@dataclass
class StreamsInfo(object):
    video_stream_url: str
    audio_stream_url: str
    is_audio_video_stream: bool
