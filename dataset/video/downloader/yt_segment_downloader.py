from dataset.video.downloader.helpers.ffmpeg_downloader import FfmpegDownloader
from dataset.video.downloader.helpers.ffmpeg_streams_merger import FfmpegStreamsMerger
from dataset.video.downloader.helpers.yt_stream_getter import YtStreamGetter
from dataset.video.downloader.segment_downloader_interface import (
    SegmentDownloaderInterface,
)
from dataset.video.downloader.supplementary_types import (
    DownloaderOptions,
    StreamsInconsistency,
)
from logger import logger


class YtSegmentDownloader(SegmentDownloaderInterface):
    def __init__(
        self,
        stream_getter=YtStreamGetter(),
        download_helper=FfmpegDownloader(),
        streams_merger=FfmpegStreamsMerger(),
    ):
        self._stream_getter = stream_getter
        self._download_helper = download_helper
        self._streams_merger = streams_merger

    def set_cookiefile(self, yt_cookiefile_path):
        self._stream_getter.set_cookiefile(yt_cookiefile_path)

    def download(
        self,
        identifier: str,
        video_url: str,
        segment_start_seconds: float,
        segment_end_seconds: float,
        options: DownloaderOptions = DownloaderOptions(),
    ) -> str:
        logger.info(
            f"identifier: {identifier}, video_url: {video_url},"
            f" segment_start_seconds: {segment_start_seconds}, segment_end_seconds: {segment_end_seconds},"
            f" options: {options}"
        )
        self._validate_options(options)
        streams_info = self._stream_getter.get_streams(video_url)
        segments_paths = self._download_helper.download_segment(
            identifier,
            streams_info,
            segment_start_seconds,
            segment_end_seconds,
            options,
        )

        logger.info(f"segment_paths: {segments_paths}")

        if len(segments_paths) == 0:
            return ""

        return self._segments_postprocessing(segments_paths, streams_info, options)

    def _segments_postprocessing(self, segments_paths, streams_info, options):
        if streams_info.is_audio_video_stream:
            return segments_paths[0]
        elif options.download_audio and options.download_video:
            if len(segments_paths) != 2:
                raise StreamsInconsistency(
                    f"Expected segments num: 2, actual: {len(segments_paths)}"
                )
            return self._streams_merger.merge_video_and_audio(segments_paths)
        else:
            return segments_paths[0]
