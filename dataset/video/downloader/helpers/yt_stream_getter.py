from youtube_dl import YoutubeDL, utils

from dataset.video.downloader.supplementary_types import StreamsInfo
from logger import logger


class StreamGetterError(Exception):
    pass


class YtStreamGetter(object):
    YTDL_OPTIONS = {
        "usenetrc": False,
        "username": None,
        "password": None,
        "twofactor": None,
        "videopassword": None,
        "ap_mso": None,
        "ap_username": None,
        "ap_password": None,
        "quiet": True,
        "no_warnings": False,
        "forceurl": True,
        "forcetitle": False,
        "forceid": False,
        "forcethumbnail": False,
        "forcedescription": False,
        "forceduration": False,
        "forcefilename": False,
        "forceformat": False,
        "forcejson": False,
        "dump_single_json": False,
        "simulate": True,
        "skip_download": False,
        "format": "bestvideo+bestaudio",
        "listformats": None,
        "outtmpl": "%(title)s-%(id)s.%(ext)s",
        "autonumber_size": None,
        "autonumber_start": 1,
        "restrictfilenames": False,
        "ignoreerrors": False,
        "force_generic_extractor": False,
        "ratelimit": None,
        "nooverwrites": False,
        "retries": 10,
        "fragment_retries": 10,
        "skip_unavailable_fragments": True,
        "keep_fragments": False,
        "buffersize": 1024,
        "noresizebuffer": False,
        "http_chunk_size": None,
        "continuedl": True,
        "noprogress": False,
        "progress_with_newline": False,
        "playliststart": 1,
        "playlistend": None,
        "playlistreverse": None,
        "playlistrandom": None,
        "noplaylist": False,
        "logtostderr": False,
        "consoletitle": False,
        "nopart": False,
        "updatetime": True,
        "writedescription": False,
        "writeannotations": False,
        "writeinfojson": False,
        "writethumbnail": False,
        "write_all_thumbnails": False,
        "writesubtitles": False,
        "writeautomaticsub": False,
        "allsubtitles": False,
        "listsubtitles": False,
        "subtitlesformat": "best",
        "subtitleslangs": [],
        "matchtitle": None,
        "rejecttitle": None,
        "max_downloads": None,
        "prefer_free_formats": False,
        "verbose": False,
        "dump_intermediate_pages": False,
        "write_pages": False,
        "test": False,
        "keepvideo": False,
        "min_filesize": None,
        "max_filesize": None,
        "min_views": None,
        "max_views": None,
        "daterange": utils.DateRange(start=None, end=None),
        "cachedir": None,
        "youtube_print_sig_code": False,
        "age_limit": None,
        "download_archive": None,
        "cookiefile": None,
        "nocheckcertificate": False,
        "prefer_insecure": None,
        "proxy": None,
        "socket_timeout": None,
        "bidi_workaround": None,
        "debug_printtraffic": False,
        "prefer_ffmpeg": None,
        "include_ads": None,
        "default_search": None,
        "youtube_include_dash_manifest": True,
        "encoding": None,
        "extract_flat": False,
        "mark_watched": False,
        "merge_output_format": None,
        "postprocessors": [],
        "fixup": "detect_or_warn",
        "source_address": None,
        "call_home": False,
        "sleep_interval": None,
        "max_sleep_interval": None,
        "external_downloader": None,
        "list_thumbnails": False,
        "playlist_items": None,
        "xattr_set_filesize": None,
        "match_filter": None,
        "no_color": False,
        "ffmpeg_location": None,
        "hls_prefer_native": None,
        "hls_use_mpegts": None,
        "external_downloader_args": None,
        "postprocessor_args": None,
        "cn_verification_proxy": None,
        "geo_verification_proxy": None,
        "config_location": None,
        "geo_bypass": True,
        "geo_bypass_country": None,
        "geo_bypass_ip_block": None,
        "autonumber": None,
        "usetitle": None,
    }

    def __init__(self, cookiefile_path=None, trials_num=5):
        self._options = {k: v for k, v in YtStreamGetter.YTDL_OPTIONS.items()}
        self._options["cookiefile"] = cookiefile_path
        self._trials_num = trials_num

    def get_streams(self, video_url: str) -> StreamsInfo:
        try:
            return self._try_do_get_streams_info(video_url)
        except KeyError as e:
            raise StreamGetterError(
                f'Failed to gather requested formats: {YtStreamGetter.YTDL_OPTIONS["format"]}'
            ) from e

    def _try_do_get_streams_info(self, video_url):
        trials = self._trials_num
        while trials > 0:
            with YoutubeDL(self._options) as yt_dl:
                logger.debug(f"trials left: {trials}")
                try:
                    media_info = yt_dl.extract_info(
                        video_url, force_generic_extractor=False
                    )
                    return self._make_streams_info(media_info["requested_formats"])
                except Exception as e:
                    logger.error(f"_try_do_get_streams_info() Error: {e}")
                    trials -= 1

        raise StreamGetterError(f"Could not get streams info for URL: {video_url}")

    def _make_streams_info(self, requested_formats):
        def is_null_codec(codec):
            return codec is None or codec == "" or codec.lower() == "none"

        video_stream_url = ""
        audio_stream_url = ""

        for media_format in requested_formats:
            if not is_null_codec(media_format["vcodec"]):
                video_stream_url = media_format["url"]

            if not is_null_codec(media_format["acodec"]):
                audio_stream_url = media_format["url"]

        logger.info(
            f"\n    video_stream_url: {video_stream_url}"
            f"\n    audio_stream_url: {audio_stream_url}"
        )

        return StreamsInfo(
            video_stream_url=video_stream_url,
            audio_stream_url=audio_stream_url,
            is_audio_video_stream=(video_stream_url == audio_stream_url),
        )

    def set_cookiefile(self, yt_cookiefile_path):
        self._options["cookiefile"] = yt_cookiefile_path
