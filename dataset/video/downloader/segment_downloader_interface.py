from abc import ABC, abstractmethod

from dataset.video.downloader.supplementary_types import DownloaderOptions


class InvalidDownloaderOptions(Exception):
    pass


class SegmentDownloaderInterface(ABC):
    @abstractmethod
    def download(
        self,
        identifier: str,
        video_url: str,
        segment_start_seconds: float,
        segment_end_seconds: float,
        options: DownloaderOptions,
    ) -> str:
        pass

    def _validate_options(self, options):
        if not options.download_video and not options.download_audio:
            raise InvalidDownloaderOptions(
                "At least one media type (video / audio) must be selected for download"
            )

    @abstractmethod
    def set_cookiefile(self, yt_cookiefile_path):
        pass
