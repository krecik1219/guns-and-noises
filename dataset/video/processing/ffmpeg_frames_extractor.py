import glob
import os
import subprocess
from typing import List

from dataset.video.processing.frames_extractor_interface import (
    FramesExtractorError,
    FramesExtractorInterface,
)
from logger import logger


class FfmpegFramesExtractor(FramesExtractorInterface):
    def extract_frames(self, video_path: str, frames_per_second: float) -> List[str]:
        logger.info(f"video_path: {video_path}, frames_per_second: {frames_per_second}")
        video_dirname = os.path.dirname(video_path)
        video_basename = os.path.basename(video_path)
        output_name_template = f'{video_basename.split(".")[0]}_%04d.jpg'
        output_name_pattern = os.path.join(video_dirname, output_name_template)
        cmd = f'ffmpeg -y -i "{video_path}" -r {frames_per_second} "{output_name_pattern}"'
        logger.info(f"calling: {cmd}")
        with subprocess.Popen(
            cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT
        ) as proc:
            proc_stdout, _ = proc.communicate()
            logger.info(proc_stdout)
            if proc.returncode != 0:
                raise FramesExtractorError(
                    f"Non zero return code: {proc.returncode} from ffmpeg cmd: {cmd}"
                )

        glob_name_template = f'{video_basename.split(".")[0]}_*.jpg'
        glob_pattern = os.path.join(video_dirname, glob_name_template)
        return sorted(glob.glob(glob_pattern))
