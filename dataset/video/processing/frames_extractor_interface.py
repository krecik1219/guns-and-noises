from abc import ABC, abstractmethod
from typing import List


class FramesExtractorError(Exception):
    pass


class FramesExtractorInterface(ABC):
    @abstractmethod
    def extract_frames(self, video_path: str, frames_per_second: float) -> List[str]:
        pass
