import sys
from pathlib import Path

from dataset.processing.videos_to_frames_converter import VideosToFramesConverter
from logger import logger_init


def run_convert_videos_to_frames(path_to_csv, output_dir, cookiefile_path):
    logger_init("convert_videos_to_frames")
    Path(output_dir).mkdir(parents=True, exist_ok=True)
    converter = VideosToFramesConverter(yt_cookiefile_path=cookiefile_path)
    converter.process_dataset(path_to_csv, output_dir)


if __name__ == "__main__":
    if len(sys.argv) < 3:
        print(
            "Please provide path to csv file and output directory as arguments, optionally YT cookiefile path"
        )
        sys.exit(1)

    path_to_csv = sys.argv[1]
    output_dir = sys.argv[2]

    cookiefile_path = None
    if len(sys.argv) >= 4:
        cookiefile_path = sys.argv[3]

    run_convert_videos_to_frames(path_to_csv, output_dir, cookiefile_path)
