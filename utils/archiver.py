import os
import zipfile


class Archiver(object):
    def compress(
        self,
        files_list,
        archive_name,
        archive_output_dir,
        remove_original_files=False,
        compression_level=9,
    ):
        archive_basename = archive_name + ".zip"
        archive_path = os.path.join(archive_output_dir, archive_basename)
        with zipfile.ZipFile(
            archive_path, "w", zipfile.ZIP_DEFLATED, compresslevel=compression_level
        ) as zip_file:
            for file_path in files_list:
                zip_file.write(file_path, os.path.basename(file_path))

        if remove_original_files:
            for file_path in files_list:
                os.remove(file_path)

        return archive_path
