import logging
import os
from datetime import datetime

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def create_logs_dir(dirpath):
    if not os.path.exists(dirpath):
        os.makedirs(dirpath)


def logger_init(logfile_name):
    base_dir = os.path.dirname(__file__)
    logs_dir = os.path.dirname(os.path.join(base_dir, "logs/"))
    create_logs_dir(os.path.dirname(os.path.join(base_dir, "logs/")))
    logfile_path = (
        f"{logs_dir}/{logfile_name}-{datetime.now().strftime('%H_%M_%d_%m_%Y')}.log"
    )

    formatter = logging.Formatter(
        "%(asctime)s %(levelname)-8s %(filename)15s:%(lineno)s-function: %(funcName)-15s]: %(message)2s",
        "%Y-%m-%d %H:%M:%S",
    )
    for handler in (
        logging.StreamHandler(),
        logging.FileHandler(logfile_path, "a", encoding="utf-8", delay="true"),
    ):
        handler.setLevel(logging.DEBUG)
        handler.setFormatter(formatter)
        logger.addHandler(handler)
    logger.debug("Logger initialized")
