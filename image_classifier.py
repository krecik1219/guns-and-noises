import mxnet as mx
from gluoncv.data.transforms.presets.imagenet import transform_eval
from gluoncv.model_zoo import get_model


class GunOnImageDetector:
    shotProbableClasses = [
        "rifle",
        "six-gun",
        "missile",
        "revolver",
        "assault gun",
        "six-shooter",
        "assault rifle",
        "projectile",
    ]

    def __init__(
        self, model: str = "ResNet50_v1d", topk: int = 4, treshold: float = 0.25
    ):
        """
        For more models please visit: https://mxnet.apache.org

        Parameters
        ----------
        model: str
            Model which will be used for detecting guns on image
        topk: int
            Amount of top predicted models to be used in detection
        treshold: float
            Value in range [0.01,1]

        """
        if treshold < 0.01 or treshold > 1:
            raise ValueError("Value of treshold should be in range [0.01, 1]")

        self.topk = topk
        self.treshold = treshold
        self.model = get_model(model, pretrained=True)

    def detect(self, path_to_image: str = None):
        image = mx.image.imread(path_to_image)
        image = transform_eval(image)
        predictions = self.model(image)
        probability = mx.nd.softmax(predictions)[0].asnumpy()
        indexes = (
            mx.nd.topk(predictions, k=self.topk).astype("int").asnumpy().tolist()[0]
        )
        return self.isShotProbable(probability, indexes)

    def isShotProbable(self, probability, indexes):
        for index in indexes:
            if (
                self.model.classes[index] in self.shotProbableClasses
                and probability[index] >= self.treshold
            ):
                return True
        return False
